﻿// 讀取menu畫面
$(document).ready(function () {
    $("#menu").load("menu.html");
});

// 打開手機menu
function openNav() {
    document.getElementById("navbarSupportedContent").style.width = "100%";
}

// 關閉手機menu
function closeNav() {
    document.getElementById("navbarSupportedContent").style.width = "0%";
}

// 滑鼠滑到menu時會跳出東西
function showAboutDropdown() {
    document.getElementById("about-dropdown").style.display = "block";
}

// 滑鼠滑到menu時會跳出東西
function closeAboutDropdown() {
    document.getElementById("about-dropdown").style.display = "none";
}


// 滑鼠滑到menu時會跳出東西
function showWorkDropdown() {
    document.getElementById("work-dropdown").style.display = "block";
}

// 滑鼠滑到menu時會跳出東西
function closeWorkDropdown() {
    document.getElementById("work-dropdown").style.display = "none";
}


// 滑鼠滑到menu時會跳出東西
function showContactDropdown() {
    document.getElementById("contact-dropdown").style.display = "block";
}

// 滑鼠滑到menu時會跳出東西
function closeContactDropdown() {
    document.getElementById("contact-dropdown").style.display = "none";
}