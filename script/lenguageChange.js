// SESSION函式
if (JSON && JSON.stringify && JSON.parse) var Session = Session || (function () {
    // cache window 物件
    var win = window.top || window;
    // 將資料都存入 window.name 這個 property
    var store = (win.name ? JSON.parse(win.name) : {});
    // 將要存入的資料轉成 json 格式
    function Save() {
        win.name = JSON.stringify(store);
    };
    // 在頁面 unload 的時候將資料存入 window.name
    if (window.addEventListener) window.addEventListener("unload", Save, false);
    else if (window.attachEvent) window.attachEvent("onunload", Save);
    else window.onunload = Save;
    // public methods
    return {
        // 設定一個 session 變數
        set: function (name, value) {
            store[name] = value;
        },
        // 列出指定的 session 資料
        get: function (name) {
            return (store[name] ? store[name] : undefined);
        },
        // 清除資料 ( session )
        clear: function () { store = {}; },
        // 列出所有存入的資料
        dump: function () { return JSON.stringify(store); }
    };
})();

$(function () {
    var dictionary, set_lang;
    var lan = Session.get("language");

    // 翻譯字典
    dictionary = {
        "chinese": {
            // menu翻譯
            "_menu.home": "首頁",
            "_menu.productService": "產品服務",
            "_menu.smartAI": "人工智慧AI",
            "_menu.forceLearning": "強化學習",
            "_menu.AR": "擴增實境",
            "_menu.screen": "客製化螢幕整合",
            "_menu.aboutUs": "關於我們",
            "_menu.ContactUs": "聯絡我們",
            // 主頁翻譯
            "_index.firstPage01": "走向智慧",
            "_index.firstPage02": "漫步雲端",
            "_index.firstPage03": "ＡＩ技術",
            "_index.firstPage04": "趨之若鶩",
            "_index.smartAIconcept.title": "SmartCloud AI 概念",
            "_index.smartAIconcept": "藉由AI深度學習以及AR的結合，提供給您虛擬中的預想畫面，並且協助您進行分析，打造專屬於您的智慧平台。",
            "_index.smartAI.title": "人工智慧",
            "_index.smartAI": "透過深度學習一層一層發現您的需求，快到不能再快，只屬於您。",
            "_index.AR.title": "擴增實境",
            "_index.AR": "讓您在虛擬和現實之間快樂暢遊，提供給您不平凡的感受和體驗。",
            "_index.screen.title": "客製化螢幕整合技術",
            "_index.screen": "創造只屬於您的螢幕，將會是獨一無二",
            // 產品服務翻譯
            "_productService.SmartCloudAI.head": "SmartCloud AI",
            "_productService.imageRegonize.title": "影像辨識",
            "_productService.imageRegonize": "提供偵測和辨識影像的技術，讓企業可以檢測產品的良率或是進行定位和搜尋，減少人力成本，增加企業的生產效能。",
            "_productService.voiceRegonize.title": "語音辨識",
            "_productService.voiceRegonize": "提供虛擬語音助理，可以輸入語音執行指令、協助企業行事曆提醒以及智能問答等智能語音應用。",
            "_productService.digitAnalysis.title": "數據分析",
            "_productService.digitAnalysis": "透過深度學習協助企業進行資料分析，成為您決策的最佳助手",
            "_productService.forceLearning.head": "強化學習",
            "_productService.UnitySurrounding.title": "Unity環境模擬",
            "_productService.UnitySurrounding": "透過Unity搭建虛擬場景提供機器人進行訓練，加速機器人的訓練速度，快速提供企業需要的服務。",
            "_productService.machinePredict.title": "機器的預測與維護",
            "_productService.machinePredict": "協助工廠設備的檢測和維修，透過物件辨識結合擴增實境，可以立即偵測損壞問題並且透過擴增實境進行遠端操作，立即解決並修正問題。",
            "_productService.objectRegonize.title": "物品辨識",
            "_productService.objectRegonize": "透過轉移學習以及擴增實境的結合，將AR眼鏡置入影像辨識，提供使用者進行資料分析等其他資訊，透過虛擬影像，讓企業能即時獲得設備或產品資訊。",
            "_productService.AR.head": "AR擴增實境，UNITY專案開發",
            "_productService.modelDevelopment.title": "客製化的模型開發",
            "_productService.modelDevelopment": "為企業提供客製化的3D虛擬模型以及場景搭建，可用於遊戲以及動畫方面的技術應用，讓企業在螢幕上觀看也能感受其中。",
            "_productService.UnityEngine.title": "UNITY遊戲引擎",
            "_productService.UnityEngine": "本產品支援PhysX 物理引擎粒子系統，並且提供網路多人連線的功能，讓您不需要學習複雜的程式語言，即可製作出遊戲上的各項需求，適合個人或小型團隊製作遊戲，有效縮短Unity遊戲的開發時間。",
            "_productService.interactiveObject.title": "互動式物件管理系統",
            "_productService.interactiveObject": "透過手部動作偵測套件Leaption，並且客製化架設產品的3D模型，讓使用者透過手勢發送指令以及360度的瀏覽企業產品，適用於產品瀏覽以及虛擬按鍵操作。",
            "_productService.headAR.title": "頭戴式AR裝置",
            "_productService.headAR": "提供AR眼鏡的技術應用，可依照企業的需求去提供適合的AR眼鏡，顯示高畫質、高效能的作業環境，企業可用於員工教育訓練或是預測性維修的操作，讓虛擬和現實世界沒有距離。",
            "_productService.screen.head": "客製化螢幕整合",
            "_productService.screen.title": "客製化螢幕整合",
            "_productService.screen": "提供企業客製化的螢幕製作，從顯示器到解碼皆可依照需求進行製作，透過討論與溝通，了解企業的訴求，提供最符合企業夥伴需求的客製化螢幕。從外到內，打造屬於企業獨一無二的專屬屏幕。",
            // 關於我們翻譯
            "_aboutUs.aboutUs.title": "關於我們",
            "_aboutUs.companyCreate.title": "公司創立",
            "_aboutUs.companyCreate": "智慧雲端科技創立於2014年，對智慧雲端團隊而言「專業是一種樂趣，追求最佳化的系統整合與效率正是技術團隊最大的樂趣」，本公司團隊專注於人工智慧以及擴增實境的技術，擁有專業的工程師與系統開發夥伴，期許本公司的團隊以及技術能夠為台灣企業及科技發展盡一分心力，也期望能夠伴隨客戶成長，成為客戶永續經營的好伙伴，透過科技所需，為企業和人民的生活變得更加便利美好正是智慧雲端的核心價值。",
            "_aboutUs.companyAppeal.title": "公司訴求",
            "_aboutUs.companyAppeal": "隨著人工智慧的趨勢與發展，智慧雲端團隊也致力於將人工智慧發展出更多不同的應用，以專業諮詢與規劃能力，提供客製化、軟硬體完美整合的解決方案與智慧設計開發，藉由不斷地討論和溝通，期望能夠做出最符合企業夥伴的產品，並且人工智慧正是智慧雲端科技第一個與客戶合作所開發出來的第一個以自有品牌推出的產品。這項系統的設計，以符合各企業需求為最優先考量，並以各企業可負擔之成本作為預算目標進行設計開發，訴求簡單、低價、實用之智慧系統，同時也提供未來客製化及擴充升級的設計空間，可針對不同顧客之需求作必要之系統功能之擴充與調整，希望透過我們的專業技術，能為企業帶來最大效益。",
            "_aboutUs.perseverance.title": "永續成長的技術與服務",
            "_aboutUs.perseverance": "『瞭解客戶的需求、迅速解決客戶問題、建立客戶的信賴』是智慧雲端科技追求穩健成長的原動力；智慧雲端熱愛科技, 善於解決智慧系統的疑難雜症, 更樂於提供客戶所需要的系統服務，也期許成為客戶長期合作的夥伴, 以專業素養為客戶提供效率更精進的技術, 服務與解決方案。",
            "_aboutUs.howToContact.title": "聯絡方式",
            "_aboutUs.contactCellphone.title": "連絡電話",
            "_aboutUs.mapTaoyuan.title": "公司地址(桃園)",
            "_aboutUs.mapTaoyuan": "桃園市龜山區萬壽路一段300號",
            "_aboutUs.mapTaichung.title": "公司地址(台中)",
            "_aboutUs.mapTaichung": "台中市西屯區文華路138巷6號2樓",
            "_aboutUs.mapTaipei.title": "公司地址(台北)",
            "_aboutUs.mapTaipei": "台北市信義區基隆路二段23號5樓之1",
            "_aboutUs.connectUs.title": "聯絡我們",
            "_aboutUs.connectUs": "任何問題都歡迎您透過以下表單詢問，我們會盡快回覆您！",
            "_aboutUs.form.name": "姓名/單位",
            "_aboutUs.form.email": "信箱",
            "_aboutUs.form.cell": "電話",
            "_aboutUs.form.message": "留言"
        },
        "english": {
            // menu英文翻譯
            "_menu.home": "Home",
            "_menu.productService": "Product service",
            "_menu.smartAI": "SmartCloud AI",
            "_menu.forceLearning": "Reinforcement Learning",
            "_menu.AR": "Augmented Reality",
            "_menu.screen": "Customized Screen Integration",
            "_menu.aboutUs": "About Us",
            "_menu.ContactUs": "Contact Us",
            // 主頁英文翻譯
            "_index.firstPage01": "Want to be smart",
            "_index.firstPage02": "Want to be top",
            "_index.firstPage03": "Advanced Cloud is your first choice",
            "_index.firstPage04": "Our technology is the best option",
            "_index.smartAIconcept.title": "SmartCloud AI Concept",
            "_index.smartAIconcept": "With the combination of AI deep learning and AR, we provide you with the vision in the virtual and help you analyze and build your own Artificial intelligence platform.",
            "_index.smartAI.title": "Artificial Intelligence",
            "_index.smartAI": "Layer by layer, discover your needs through deep learning, faster than you can ever imagined, designed specifically for you.",
            "_index.AR.title": "Augmented Reality",
            "_index.AR": "Let you enjoy a pleasant journey between virtual and reality, providing you with an extraordinary feeling and experience.",
            "_index.screen.title": "Customized Screen Integration Technology",
            "_index.screen": "Create a unique screen that only belongs to you",
            // 產品服務英文翻譯
            "_productService.SmartCloudAI.head": "SmartCloud AI",
            "_productService.imageRegonize.title": "Image Recognition",
            "_productService.imageRegonize": "Provides technology for detecting and identifying images, assist companies to detect product yields or locate and search, reduce labor costs and improve production efficiency.",
            "_productService.voiceRegonize.title": "Speech Recognition",
            "_productService.voiceRegonize": "Provides a virtual voice assistant that can input voice execution commands, assist with calendar reminders, and intelligent voice applications such as questions and answers.",
            "_productService.digitAnalysis.title": "Data Analysis",
            "_productService.digitAnalysis": "Provide assistance to the company for data analysis through deep learning and become the best assistant for your decision-making",
            "_productService.forceLearning.head": "Reinforcement Learning",
            "_productService.UnitySurrounding.title": "Unity Environment Simulation",
            "_productService.UnitySurrounding": "Use Unity to build virtual scenes to provide robots training, speed up robot training, and quickly provide the services which businesses need.",
            "_productService.machinePredict.title": "Machine Prediction and Maintenance",
            "_productService.machinePredict": "Assist in the inspection and maintenance of factory equipment, through object identification combined with augmented reality, can immediately detect the damage problem and remotely operate through the augmented reality to solve and correct the problem immediately.",
            "_productService.objectRegonize.title": "Item Identification",
            "_productService.objectRegonize": "Through the combination of transfer learning and augmented reality, AR glasses are placed into image recognition, Providing users with other information such as data analysis, and through virtual images, Enterprises can instantly obtain the information on equipment or product.",
            "_productService.AR.head": "Augmented Reality, UNITY Project Development",
            "_productService.modelDevelopment.title": "Customized Model Development",
            "_productService.modelDevelopment": "Provide customized 3D virtual models and scenes for enterprises, which can be used in game and animation technology applications, so that enterprises can also experience it by viewing the screen.",
            "_productService.UnityEngine.title": "UNITY Game Engine",
            "_productService.UnityEngine": "This product supports the PhysX physics engine particle system, and provides the function of network multi-person connection, so that you can create various game requirements without learning complex programming language. Suitable for individual or small teams to create games, effectively reducing the development time of Unity games.",
            "_productService.interactiveObject.title": "Interactive Object Management System",
            "_productService.interactiveObject": "Through the hand motion detection suite Leaption, and the customized 3D model of the product, the user can send instructions through gestures and 360-degree browsing of the enterprise's products, suitable for product browsing and virtual button operations.",
            "_productService.headAR.title": "Head Mounted AR Device",
            "_productService.headAR": "Provide technical application of AR glasses, provide suitable AR glasses according to the needs of enterprises, display high-quality, high-efficient working environment, and enterprises can be used for employee education training or predictive maintenance operations. There is no distance between virtual and reality.",
            "_productService.screen.head": "Customized Screen Integration",
            "_productService.screen.title": "Customized Screen Integration",
            "_productService.screen": "Provide customized screen production, from display to decoding, can be produced according to the needs, through discussion and communication, to understand the company's needs, to provide a customized screen that which is the best fit for the company's needs. From the outside to the inside, create a unique screen design for your company.",
            // 關於我們英文翻譯
            "_aboutUs.aboutUs.title": "About us",
            "_aboutUs.companyCreate.title": "Our History",
            "_aboutUs.companyCreate": "Advanced Cloud Technology was founded in 2014. For the Smart Cloud team, “Professional is a pleasure, and the greatest pleasure comes from the pursuit of optimal system integration and efficiency is the technical team”. Our team focuses on artificial intelligence and augmented reality technology, with professional engineers and system development partners, and expected to the company's team and technology can make a contribution to Taiwan's enterprise and technology development. It is also expected to be able to grow with clients and become a good partner for client' sustainable operation. It is the core value of the Advanced Cloud is to bring convenience to the life of the company and the people through the needs of technology.",
            "_aboutUs.companyAppeal.title": "Company product appeal",
            "_aboutUs.companyAppeal": "With the trend and development of artificial intelligence, the Advanced Cloud team is also committed to developing artificial intelligence into more diverse applications,  with professional consulting and planning capabilities, providing customized, software and hardware integration solutions and intelligent design development. Through continuous discussion and communication, we hope to make the products which are the best for our business partners, and artificial intelligence is the first product developed by Advanced Cloud Technology to cooperate with clients and launched under our own brand. The system is designed to fit the needs of each enterprise as our top priority, and the cost of design and development based on each enterprise’s budget, appealing to simple, low-cost, practical Intelligent system, and also provides space for future customization, expansion and upgrading. We can expand and adjust the system functions according to the needs of different clients.We hope to bring maximum benefit to our clients through our professional technology.",
            "_aboutUs.perseverance.title": "Sustainable technology and services",
            "_aboutUs.perseverance": "\"Understanding clients’ needs, quickly solving clients’ problems, and building clients’ trust\" is the driving force behind Advanced Cloud Technology's pursuit of steady growth;  Advanced Cloud is passionate about technology and is expert in solving the intractable diseases of the Intelligent system. We are willing to provide the system services that our clients need.We also except ourselves to become a long-term partner of our clients, and provide customers with more efficient technologies, services and solutions with professionalism.",
            "_aboutUs.howToContact.title": "Contact Way",
            "_aboutUs.contactCellphone.title": "Phone number",
            "_aboutUs.mapTaoyuan.title": "Company Address (Taoyuan)",
            "_aboutUs.mapTaoyuan": "No.300, Wanshou Rd, Guishan Dist., Taoyuan City 333, Taiwan (R.O.C.)",
            "_aboutUs.mapTaichung.title": "Company Address (Taichung)",
            "_aboutUs.mapTaichung": "2F., No.6, Ln. 138, Wenhua Rd., Xitun Dist., Taichung City 407, Taiwan (R.O.C.)",
            "_aboutUs.mapTaipei.title": "Company Address (Taipei)",
            "_aboutUs.mapTaipei": "Rm. 1, 5F., No.23, Sec. 2, Keelung Rd., Xinyi Dist., Taipei City 110, Taiwan (R.O.C.)",
            "_aboutUs.connectUs.title": "Contact Us",
            "_aboutUs.connectUs": "If you have any questions about us, please fill in the form below and we will reply you as soon as possible.",
            "_aboutUs.form.name": "Name/Unit",
            "_aboutUs.form.email": "Email",
            "_aboutUs.form.cell": "Phone number",
            "_aboutUs.form.message": "Message"
        }
    };

    // Function for swapping dictionaries
    set_lang = function (dictionary) {
        $("[data-translate]").text(function () {
            var key = $(this).data("translate");
            if (dictionary.hasOwnProperty(key)) {
                return dictionary[key];
            }
        });
        $("form").find("textarea").text("");
        $("[data-translate]").attr("placeholder", function () {
            var key = $(this).data("translate");
            if (dictionary.hasOwnProperty(key)) {
                return dictionary[key];
            }
        });
    };

    // 按下按鈕，切換語言
    $(".lang").click(function () {
        if (lan == "english") {
            lan = "chinese";
            Session.set("language", "chinese");
            set_lang(dictionary.chinese);
            //$(".content-title").css({ "line-height": "2.1em" });
            //$(".content-text").css({ "line-height": "1.6em" });
            console.log(Session.get("language"));
        }
        else {
            lan = "english";
            Session.set("language", "english");
            set_lang(dictionary.english);
            //$(".content-title").css({ "line-height": "1.0em" });
            //$(".content-text").css({ "line-height": "1.0em" });
            console.log(Session.get("language"));
        }
        window.location.href = window.location.href; //This is a possibility
        window.location.reload(); //Another possiblity
        history.go(0); //And another
    });

    // 設定初始語言
    if (lan == "" || lan == "chinese") {
        set_lang(dictionary.chinese);
    }
    else if (lan == "english") {
        set_lang(dictionary.english);
    }
});