﻿/*
$(document).ready(function () {
    $("#firstBox").html("舒服");
    $("#secondBox").html("舒服");
    $("#thirdBox").html("讚");
})*/
// 主頁手機板變化(針對productBox能不能點擊做變化)
$(document).ready(function () {
    // 一開始畫面判斷
    var w = $(window).width();
    if (w <= 768) {
        var productBox = document.getElementsByClassName("BoxButton");
        for (i = 0; i < productBox.length; i++){
            productBox[i].setAttribute("onclick", "null");
        }
    }
    else {
        var productBox = document.getElementsByClassName("BoxButton");
        for (i = 0; i < productBox.length; i++) {
            productBox[i].setAttribute("onclick", "openPrd(" + i + ")");
        }
    }
    // 頁面縮減判斷
    $(window).resize(function () {
        var w = $(window).width();
        if (w <= 768) {
            var productBox = document.getElementsByClassName("BoxButton");
            for (i = 0; i < productBox.length; i++) {
                productBox[i].setAttribute("onclick", "null");
            }
        }
        else {
            var productBox = document.getElementsByClassName("BoxButton");
            for (i = 0; i < productBox.length; i++) {
                productBox[i].setAttribute("onclick", "openPrd(" + i + ")");
            }
        }
    });
});


function openPrd(n) {
    changeProductBox(n);
    document.getElementById("productBox").style.display = "block";   
}

function closePrd() {
    document.getElementById("productBox").style.display = "none";
}

function changeProductBox(n) {
    var topImg = document.getElementById("topImg");
    var leftImg = document.getElementById("leftImg");
    var rightImg = document.getElementById("rightImg");
    var leftImgText = document.getElementById("leftImgText");
    var rightImgText = document.getElementById("rightImgText");
    var title = document.getElementById("product-content-title");
    var subtitle = document.getElementById("product-content-subtitle");
    var text = document.getElementById("product-content-text");

    // box 1
    if (n == 0) {
        topImg.setAttribute("src", "Image/AR 預測維護系統-1.png");
        leftImg.setAttribute("src", "Image/AR 預測維護系統-2.png");
        rightImg.setAttribute("src", "Image/AR 預測維護系統-3.png");
        leftImgText.innerHTML = "▲ 登入畫面";
        rightImgText.innerHTML = "▲ 管理頁面"
        title.innerHTML = "AR 預測維護系統";
        subtitle.innerHTML = "利用智慧化系統來降低生產成本及提升效率。";
        text.innerHTML = "本系統由AR眼鏡與電腦組成，提供與產品互動進行虛實互動功能，使用者可透過虛擬畫面了解產品各部分詳細資訊。<br/><br/>使用者無需了解產品型號，系統可透過辨識功能直接顯示相對應產品資訊，細部資訊則可透過資料庫取得產品資料及生命週期等數據。";
    }

    // box 2
    if (n == 1) {
        topImg.setAttribute("src", "Image/AOI瑕疵測試box.png");
        leftImg.setAttribute("src", "Image/AOI瑕疵測試-1.png");
        rightImg.setAttribute("src", "Image/AOI瑕疵測試-2.png");
        leftImgText.innerHTML = "▲ 搭配使用進行邊緣運算";
        rightImgText.innerHTML = "▲ 檢測使用的介面"
        title.innerHTML = "AOI 瑕疵檢測";
        subtitle.innerHTML = "利用智慧化系統來降低生產成本及提升效率。";
        text.innerHTML = "PBCPCB印刷電路板(Printed Circuit Board)，在20世紀初被廣泛應用在日常生活中，舉凡電視機、冰箱、電腦等……家電用品，面對IOT的發展，PCB的重要性越來越高，AOI可以快速篩檢出有問題的電路板。<br><br>由於AOI的標準相當嚴格，真正篩檢出來的電路板只有少部分真正存在問題。<br><br>我們公司利用人工智慧的方式架構神經網路，訓練出一套模型進行分類，搭配Nvidia的開發板Xavier進行邊緣運算，運用人工智慧的方式自動進行檢測，將可大幅提高生產效率並降低成本。";
    }

    // box 3
    if (n == 2) {
        topImg.setAttribute("src", "Image/電路板元件生命週期分析-1.png");
        leftImg.setAttribute("src", "Image/電路板元件生命週期分析-2.png");
        rightImg.setAttribute("src", "Image/電路板元件生命週期分析-3.png");
        leftImgText.innerHTML = "▲ 登入畫面";
        rightImgText.innerHTML = "▲ 管理頁面"
        title.innerHTML = "電路板元件生命週期分析";
        subtitle.innerHTML = "利用演算法及大數據預測即將損壞的零件";
        text.innerHTML = "「預測性維修」為近年泛討論的概念，目的是在電路板上的元件即將損壞前先進行更換，減少機器故障發生機率並降低因機器故障產生的損失。(預測性維修)<br><br>面對人口紅利減少及少子化威脅，電路板維修技術人員人數逐年下降，而技術人員的養成往往需要時間培養，在流動率高的公司很容易造成人口斷層。<br><br>我公司利用演算法，結合大數據分析，對電路板上每個元件進行預測、分析，訓練模型並以webpage實現溝通介面。(解決方案)";
    }
}